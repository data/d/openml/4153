# OpenML dataset: Smartphone-Based_Recognition_of_Human_Activities

https://www.openml.org/d/4153

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Jorge L. Reyes-Ortiz, Davide Anguita, Luca Oneto and Xavier Parra. Preprocessed by Mikhail Evchenko  
**Source**: [UCI](http://archive.ics.uci.edu/ml/datasets/Smartphone-Based+Recognition+of+Human+Activities+and+Postural+Transitions)  
**Please cite**: Jorge-L. Reyes-Ortiz, Luca Oneto, Albert Sama, Xavier Parra, Davide Anguita. Transition-Aware Human Activity Recognition Using Smartphones. Neurocomputing. Springer 2015.  

**Transition-Aware Human Activity Recognition**  
The experiments were carried out with a group of 30 volunteers within an age bracket of 19-48 years. They performed a protocol of activities composed of six basic activities: three static postures (standing, sitting, lying) and three dynamic activities (walking, walking downstairs and walking upstairs). The experiment also included postural transitions that occurred between the static postures. These are: stand-to-sit, sit-to-stand, sit-to-lie, lie-to-sit, stand-to-lie, and lie-to-stand. All the participants were wearing a smartphone (Samsung Galaxy S II) on the waist during the experiment execution. We captured 3-axial linear acceleration and 3-axial angular velocity at a constant rate of 50Hz using the embedded accelerometer and gyroscope of the device. The experiments were video-recorded to label the data manually.

The sensor signals (accelerometer and gyroscope) were pre-processed by applying noise filters and then sampled in fixed-width sliding windows of 2.56 sec and 50% overlap (128 readings/window). The sensor acceleration signal, which has gravitational and body motion components, was separated using a Butterworth low-pass filter into body acceleration and gravity. The gravitational force is assumed to have only low frequency components, therefore a filter with 0.3 Hz cutoff frequency was used. From each window, a vector of 561 features was obtained by calculating variables from the time and frequency domain. See 'features_info.txt' for more details. 

This dataset is an updated version of the UCI Human Activity Recognition Using smartphones Dataset that can be found at: [Web Link] 
This version provides the original raw inertial signals from the smartphone sensors, instead of the ones pre-processed into windows which were provided in version 1. This change was done in order to be able to make online tests with the raw data. Moreover, the activity labels were updated in order to include postural transitions that were not part of the previous version of the dataset. 

**Note**: From the original UCI data, the data was aggregated for each person and activity type (mean). Train and test sets are joined.

### Attribute Information  
- Raw triaxial signals from the accelerometer and gyroscope of all the trials with participants.  
- The labels of all the performed activities.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/4153) of an [OpenML dataset](https://www.openml.org/d/4153). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/4153/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/4153/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/4153/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

